<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи, язык WordPress и ABSPATH. Дополнительную информацию можно найти
 * на странице {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется сценарием создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения.
 *
 * @package WordPress
 */
define( 'CONCATENATE_SCRIPTS', false );
define('SCRIPT_DEBUG', true);
// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'eugenese');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'V3ufG0k1c');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

define('WP_POST_REVISIONS', 0);

define('CONCATENATE_SCRIPTS', false);

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется снова авторизоваться.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$6IPOU]n|n-_{B-UDF>mU:oH+n?B5WF6b6iWZ+Q/9* -K&Xl@|Kxu +E+~jsYU*s');
define('SECURE_AUTH_KEY',  '8CK_%ap8Zbeqa|~k!Cm?DhNp/|jGqpBMM.Oyfp$rzcTP2Bq=.j, ^e|L,a*5Bz_>');
define('LOGGED_IN_KEY',    '4duzqlMkVnK4qz{8UU[y#tNor$P4qHXO=|WCdgGdDf&5UsQ2^Fwi1@66E)8lS-fa');
define('NONCE_KEY',        '0x&x0zjCBUqKL|ypFWETy}v^cZwNqY86t9d[I<R] {!L|<*x@Wy/!L1JcoyuuU3O');
define('AUTH_SALT',        'm~yj.;wP@UR/&}26+yocY~Mv+MqU:yoe3N6S=+S3~S-(Z7A<6p^A|q]{@5q,C~Wt');
define('SECURE_AUTH_SALT', '4fKz1Fc3E3k(k9k8|.Pxc0X1S,*k3(xbtK1M2crnr<SdbIlGq~=YrpLzCYtt0GT2');
define('LOGGED_IN_SALT',   'vhqe(o?Gq,+O6iX1Pv7Oo##3qTHeHoOMy|u1F<t<PNT.N7Z((%#+yJ%|!;N*m| Y');
define('NONCE_SALT',       '5.QP2vAhM4|_qIz`yn>1/Hv6 1Z>;+x`o*h)oS+BhlsBbdjXf+;?z}dpF#o+=yM+');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько блогов в одну базу данных, если вы будете использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Язык локализации WordPress, по умолчанию английский.
 *
 * Измените этот параметр, чтобы настроить локализацию. Соответствующий MO-файл
 * для выбранного языка должен быть установлен в wp-content/languages. Например,
 * чтобы включить поддержку русского языка, скопируйте ru_RU.mo в wp-content/languages
 * и присвойте WPLANG значение 'ru_RU'.
 */
define('WPLANG', 'ru_RU');

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Настоятельно рекомендуется, чтобы разработчики плагинов и тем использовали WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');