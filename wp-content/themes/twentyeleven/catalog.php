<?php
/**
 * Template Name: Music Catalog Template
 *
 * A custom music catalog page template.
 *
*/

get_header(); ?>
		<div id="primary">
			<div id="content" role="main">
				<!-- Start Code -->
					<?php
					$postIDs = array();
					$pageChildren = get_pages('child_of=' . $post->ID . '&exclude=' . $exclude_page );
					if ( $pageChildren ) {
					  foreach ( $pageChildren as $pageChild ) {
						$postIDs[] = $pageChild->ID;
					  }
					  $paged = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
					  $args = array(
						'post_type' => 'page',
						'paged' => $paged,
						'post__in' => $postIDs,
						'posts_per_page' => 10,
						'orderby'  => 'menu_order',
						'order' => 'ASC'
						
									);
					  query_posts($args);
					
					  if (have_posts()) : while (have_posts()) : the_post(); ?>
					
						<div class="albums" id="post-<?php the_ID(); ?>">					  	
							<div class="album_cover">
							
							<a href="<?php the_permalink(); ?>">
								<h2 class="music_page_title"><?php the_title(); ?></h2>
								<?php the_post_thumbnail('medium'); ?>
							</a></div>
						 	<div class="br"></div>
						</div>
					  
					  <?php endwhile; ?>
					
					<?php endif; ?>
					<?php wp_simple_pagination(); ?>
					
					<?php } ?>
					<!-- End Code -->
			</div><!-- #content -->
		</div><!-- #primary -->
<?php get_footer(); ?>