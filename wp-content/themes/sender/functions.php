<?php

// Register widgetized areas

if (!function_exists( 'the_widgets_init')) {
	function the_widgets_init() {
	    if ( !function_exists( 'register_sidebar') )
	        return;
	
	    register_sidebar(array( 'name' => 'Twitter','id' => 'twitter','description' => "Twitter on top", 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));
		register_sidebar(array( 'name' => 'Soc icons','id' => 'soc-icons','description' => "Soc icons on top", 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));
		register_sidebar(array( 'name' => 'Video & subscription','id' => 'video-subscription','description' => "Video & subscription", 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>')); 
		register_sidebar(array( 'name' => 'Widgets in header','id' => 'header-widgets','description' => "Widgets in header of the page", 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));
		register_sidebar(array( 'name' => 'Footer Logos','id' => 'footer-logos','description' => "Logos in footer", 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));
		register_sidebar(array( 'name' => 'Footer Social','id' => 'footer-soc','description' => "Soc links in footer", 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));
	}
}

add_action( 'init', 'the_widgets_init' );

add_theme_support('post-thumbnails');
if ( function_exists('add_theme_support') )
add_theme_support('post-thumbnails');

add_filter( 'wp_nav_menu_items', 'first_last_class' );
function first_last_class( $items ) {
$first = strpos( $items, 'class=' );
if( false !== $first )
$items = substr_replace( $items, 'first ', $first+7, 0 );
$last = strripos( $items, 'class=');
if( false !== $last )
$items = substr_replace( $items, 'last ', $last+7, 0 );
return $items;
}

function twentyten_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'twentyten_excerpt_length' );

function twentyten_continue_reading_link() {
	return '';
}

function twentyten_auto_excerpt_more( $more ) {
return ' &hellip;' . twentyten_continue_reading_link();
}
add_filter( 'excerpt_more', 'twentyten_auto_excerpt_more' );

$url = explode('?', 'http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
$ID = url_to_postid($url[0]);

$gen1 = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.ID = ".$ID."  AND $wpdb->posts.post_type = 'page' AND $wpdb->posts.post_status = 'publish' ORDER BY $wpdb->posts.ID ASC");
$gen1_ids = implode($gen1,', ');
$gen2 = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_parent IN ($gen1_ids) AND $wpdb->posts.post_type = 'page' AND $wpdb->posts.post_status = 'publish' ORDER BY $wpdb->posts.ID ASC");
$gen2_ids = implode($gen2,', ');
$gen3 = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_parent IN ($gen2_ids) AND $wpdb->posts.post_type = 'page' AND $wpdb->posts.post_status = 'publish' ORDER BY $wpdb->posts.ID ASC");
$gen3_ids = implode($gen3,', ');

// KILL UPDATES
remove_action( 'wp_version_check', 'wp_version_check' );
remove_action( 'admin_init', '_maybe_update_core' );
add_filter( 'pre_transient_update_core', create_function( '$a', "return null;"));
add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;"));
wp_clear_scheduled_hook( 'wp_version_check' );
remove_action( 'load-plugins.php', 'wp_update_plugins' );
remove_action( 'load-update.php', 'wp_update_plugins' );
remove_action( 'load-update-core.php', 'wp_update_plugins' );
remove_action( 'admin_init', '_maybe_update_plugins' );
remove_action( 'wp_update_plugins', 'wp_update_plugins' );
add_filter( 'pre_transient_update_plugins', create_function( '$a', "return null;" ) );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
wp_clear_scheduled_hook( 'wp_update_plugins' );
remove_action( 'load-themes.php', 'wp_update_themes' );
remove_action( 'load-update.php', 'wp_update_themes' );
remove_action( 'load-update-core.php', 'wp_update_themes' );
remove_action( 'admin_init', '_maybe_update_themes' );
remove_action( 'wp_update_themes', 'wp_update_themes' );
add_filter( 'pre_transient_update_themes', create_function( '$a', "return null;" ) );
add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) );
wp_clear_scheduled_hook( 'wp_update_themes' );
// End of KILL UPDATES

// add tag support to pages
function tags_support_all() {
    register_taxonomy_for_object_type('post_tag', 'page');
}

// ensure all tags are included in queries
function tags_support_query($wp_query) {
    if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}

// tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');

function sender_setup() {

    $options = array(
        'banner_image' => array(
            'type' => 'image',
            'label' => __('Изображение для рекламного баннера', 'sender'),
            'tip' => __('Размером 728 x 90 px', 'sender')
        ),
        'banner_href' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Ссылка для рекламного баннера', 'sender'),
        ),
        'banner_facebook' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Ссылка на аккаунт Facebook', 'sender'),
        ),
        'banner_twitter' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Ссылка на аккаунт Twitter', 'sender'),
        ),
        'banner_youtube' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Ссылка на аккаунт Youtube', 'sender'),
        ),
        'banner_soundcloud' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Ссылка на аккаунт Soundcloud', 'sender'),
        ),
        'banner_itunes' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Ссылка на аккаунт iTunes', 'sender'),
        ),
        'banner_beatport' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Ссылка на аккаунт Beatport', 'sender'),
        ),
        'banner_vk' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Ссылка на аккаунт VK', 'sender'),
        ),

    );

    synved_option_register('options', $options);
}

add_action( 'after_setup_theme', 'sender_setup' );
?>