<?php get_header(); ?>
		<div class="content item">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div class="page-title"><h1><div class="media-views" style="margin-top:10px;"><?php echo do_shortcode('[post_view]'); ?></div><?php the_title();?></h1></div>			
				<?php the_content(); ?>
			<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
			<div class="blog-comment-tabber"><?php comments_template( '', true ); ?></div>
		<?php endwhile; // end of the loop. ?>
		<div class="fix"></div>
		</div><!-- .entry-content -->
				
<?php get_footer(); ?>