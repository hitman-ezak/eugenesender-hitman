<?php
/**
 * Template Name: Shop Item Template
 *
 * A custom shop item page template.
 *
*/

get_header(); ?>
		<div class="content shop item">	
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
							<?php
									$postIDs = array();
									
									$parentstop = get_post_ancestors( $post->ID );
									$idparent = ($parentstop) ? $parentstop[count($parentstop)-1]: $post->ID;
									
									$pageChildren = get_pages(
									array (
									'child_of' => $idparent,
									'parent' => $idparent,
									)
							);?>
							
							<div class="page-title">
							<h1><font style="color:#666666;"><?php echo get_the_title($idparent);?></font> &gt; <?php echo get_the_title($post->post_parent);?></h1></div>
							<div class="shop-tags">
							<!-- Start Code -->									
									<?php if ( $pageChildren ) {
									  foreach ( $pageChildren as $pageChild ) {
										$postIDs[] = $pageChild->ID;
									  }
									  $args = array(
										'post_type' => 'page',
										'post__in' => $postIDs,
										'orderby'  => 'post_date',
										'order' => 'DESC',						
										);
									  query_posts($args);
									if (have_posts()) : while (have_posts()) : the_post(); ?>
										<div class="shop-tag">
											<a href="<?php the_permalink(); ?>">
												<?php the_title(); ?>
											</a>
										</div>					  
									<?php endwhile; ?>
									<?php endif; ?>
									<div class="fix"></div>
								<?php }?>
							<?php wp_reset_query(); ?>
							<!-- End Code -->	
						</div>
							<?php if(get_field('img_or_video'))
										{
											echo '<div class="page-in-left">'. get_field('img_or_video') .'</div><div class="page-in-right">';
										}
									else {
										echo '<div class="item_in_content">';
									}
							?>

								<?php if(get_field('price'))
										{
											echo '<div class="price"><span class="number">'. get_field('price') . '</span><span class="money-type">$</span></div>';
										}
								?>
								
								<div class="parent-tag">
											<?php if ( get_post_meta($post->post_parent, 'icon_parent_tag', true) ) : ?>
												<?php $image_icon = wp_get_attachment_image_src (get_post_meta($post->post_parent, 'icon_parent_tag', true)); ?>
												<img src="<?php echo $image_icon[0]; ?>" />
											<?php endif; ?>
											<?php echo get_the_title($post->post_parent); ?> 
											<?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?>										
								</div>
								
								<h3><strong><?php the_title(); ?></strong></h3>
								<?php the_content(); ?>
								
								<?php if ( qtrans_getLanguage() == 'en' ) {
									if(get_field('button_buy'))
									{
										echo '<div class="buy-button"><a href="' . get_field('button_buy') . '" target="_blank">' . get_field('button_eng') . '</a></div>';
									}
									} else {
										if(get_field('button_buy'))
										{
											echo '<div class="buy-button"><a href="' . get_field('button_buy') . '" target="_blank">' . get_field('button_rus') . '</a></div>';
										}
									}?>
															
							</div>
							
							<div class="fix"></div>
							<?php if ( qtrans_getLanguage() == 'en' ) {
											if(get_field('buy_link'))
											{
												echo '<div class="buy_link"><h2>' . get_field('buy_links_eng') . '</h2><div class="buy_links_shops">' . get_field('buy_link') . '</div></div>';
											}
																
										} else {
											if(get_field('buy_link'))
											{
												echo '<div class="buy_link"><h2>' . get_field('buy_links_rus') . '</h2><div class="buy_links_shops">' . get_field('buy_link') . '</div></div>';
											}
							}?>
							<div class="fix"></div>			
							<!-- Start Code -->
							<ul style="margin:30px 0;">
							<?php if(get_field('pay_logos'))
								{
									echo '<div class="pay_logos">'. get_field('pay_logos') . '</div>';
								}
							?>
							<?php
									$postIDs = array();
									$pageChildren = get_pages(
									array (
									'child_of' => $post->post_parent,
									'parent' => $post->post_parent,
									'exclude' => $post->ID
									)
								);
			
								if ( $pageChildren ) {
								  foreach ( $pageChildren as $pageChild ) {
									$postIDs[] = $pageChild->ID;
								  }
								  $args = array(
									'post_type' => 'page',
									'post__in' => $postIDs,
									'orderby'  => 'post_date',
									'order' => 'DESC',
									'posts_per_page' => '3',						
									);
								  query_posts($args);
							
							  if (have_posts()) :?>
							  <?php if ( qtrans_getLanguage() == 'en' ) { ?>
								<h2 style="border:0px;">Related Items</h2>
								<?php } else { ?>
								<h2 style="border:0px;">Похожие товары</h2>
							<?php } ?>
							<?php while ( have_posts() ) : the_post();?>
									<li class="shop-content">
										<div class="parent-tag">
												<?php if ( get_post_meta($post->post_parent, 'icon_parent_tag', true) ) : ?>
													<?php $image_icon = wp_get_attachment_image_src (get_post_meta($post->post_parent, 'icon_parent_tag', true)); ?>
													<img src="<?php echo $image_icon[0]; ?>" />
												<?php endif; ?>
												<?php echo get_the_title($post->post_parent); ?> 
												<?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?>										
										</div>
										<a href="<?php the_permalink(); ?>" class="shop-link">
										<div class="shop-thumb">
											<div class="bottom">
													<?php if ( qtrans_getLanguage() == 'en' ) {
														if(get_field('button_buy'))
															{
																echo '<div class="buy-button">' . get_field('button_eng') . '</div>';
															}
														
													} else {
														if(get_field('button_buy'))
															{
																echo '<div class="buy-button">' . get_field('button_rus') . '</div>';
															}
													}
													?>
											</div>
											<div class="top"><?php the_post_thumbnail('thumbnail'); ?></div>
										</div>
											<h3><?php the_title(); ?></h3>
										</a>
										<div class="fix"></div>
	
										<?php if(get_field('price'))
											{
												echo '<div class="price"><span class="number">'. get_field('price') . '</span><span class="money-type">$</span></div>';
											}
										?>
									</li>					  
							  <?php endwhile; ?>
							
							<?php
							else :
								echo wpautop( '' );
							endif;
							?>	
							<div class="fix"></div>						
							<?php }?>
							<?php wp_reset_query(); ?>
							</ul>
						<!-- End Code -->
					<div class="blog-comment-tabber"><?php comments_template( '', true ); ?></div>
					<div class="fix"></div>
				<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>