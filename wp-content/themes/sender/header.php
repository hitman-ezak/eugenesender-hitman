<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
<?php
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
	?>
</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.ico">
<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<!--
  jQuery library
-->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery-1.4.2.min.js"></script>
<!--
  jCarousel library
-->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.jcarousel.min.js"></script>
<!--
  jCarousel skin stylesheet
-->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/gallery.css" />
<script type="text/javascript">
var $p = jQuery.noConflict();
$p(document).ready(function() {
$p('#mycarousel').jcarousel({
scroll:1
});
});
</script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery-1.8.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.liFixar.js"></script>
<script>
var $w = jQuery.noConflict();
$w(function(){
	$w(window).load(function(){
		$w('#menu').liFixar({
			relative:'top',
			fixStart:0
		});
	})

	$w( document ).ready(function() {
//		$w('.menu-item').hover(function(){
//			$w(this).children('.sub-menu').show();
//			$w(this).children('.sub-menu').addClass('showed');
//		},
//		function(e){
//			if ($w(this).children('.sub-menu').length > 0) {
//				return;
//			}
//
//			$w(this).children('.sub-menu').hide();
//			$w(this).children('.sub-menu').removeClass('showed');
//		}
//		);
})

});

var Lst;

function CngClass(obj){
 if (typeof(obj)=='string') obj=document.getElementById(obj);
 if (Lst) Lst.className='';
 obj.className='tabberactive';
 Lst=obj;
}
</script>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>

<body onLoad="CngClass('kv_s')">
	<?//php if ( is_page_template('landing-page.php') ) { ?>
<!--	<div id="header-landing">-->
<!--		<div class="soc-icons">--><?php //dynamic_sidebar( 'soc-icons' ); ?><!--</div>-->
<!--		<div id="logo"><a href="--><?php //echo home_url( '/' ); ?><!--" title="--><?php //echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?><!--" rel="home"><img src="--><?php //bloginfo('stylesheet_directory'); ?><!--/images/sender-logo.png" /></a></div>-->
<!--		<div class="twitter">--><?php //dynamic_sidebar( 'twitter' ); ?><!--</div>-->
<!--	</div>-->
<!---->
<!--	<div id="body">-->
<!--			<div id="video">-->
<!--				<div id="wrapper">-->
<!--					--><?php //dynamic_sidebar( 'video-subscription' ); ?>
<!--					<div class="fix"></div>-->
<!--				</div>-->
<!--			</div>-->
<!--		<div id="wrapper">-->
		<?//php } else { ?>
		<div id="header">
			<div class="header">
				<div class="header-widgets">
					<?php dynamic_sidebar( 'header-widgets' ); ?>
					<div class="login">
						<?php if ( qtrans_getLanguage() == 'en' ) { ?>

							<?php global $user_ID, $user_identity, $user_level ?>
							<?php if ( $user_ID ) : ?>
							<div class="logout-button"><a href="<?php echo wp_logout_url(get_permalink()); ?>">&nbsp;</a></div>
							<div class="login-avatar">
								<?php echo get_avatar($user_ID, 41); ?>
							</div>
							<div class="login-info">
								<a href="<?php echo home_url(); ?>/wp-admin/profile.php" class="login-settings">Settins Page</a>
							</div>
							<?php else : ?>

							<?php if(!is_user_logged_in()) { ?>
								<div class="login-button"><a href="/wp-login.php" class="simplemodal-login">&nbsp;</a></div>
							<?php } ?>
							<?php if ( get_option('users_can_register') ) : ?>
								<div class="login-reg"><a href="/wp-login.php?action=register" class="simplemodal-register">Register</a></div>
							<?php endif; ?>
							<?php endif; ?>

						<?php } else { ?>

							<?php global $user_ID, $user_identity, $user_level ?>
							<?php if ( $user_ID ) : ?>
							<div class="logout-button"><a href="<?php echo wp_logout_url(get_permalink()); ?>">&nbsp;</a></div>
							<div class="login-avatar">
								<?php echo get_avatar($user_ID, 41); ?>
							</div>
							<div class="login-info">
								<a href="<?php echo home_url(); ?>/wp-admin/profile.php" class="login-settings">Личный кабинет</a>
							</div>
							<?php else : ?>

							<?php if(!is_user_logged_in()) { ?>
								<div class="login-button"><a href="/wp-login.php" class="simplemodal-login">&nbsp;</a></div>
							<?php } ?>
							<?php if ( get_option('users_can_register') ) : ?>
								<div class="login-reg"><a href="/wp-login.php?action=register" class="simplemodal-register">Зарегистрироваться</a></div>
							<?php endif; ?>
							<?php endif; ?>
						<?php } ?>
					</div>
				</div>
				<div id="logo"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/sender-logo-new.png" /></a></div>
                <div id="social" style="float:left;">
                    <?php $banner_facebook = synved_option_get('options', 'banner_facebook');?>
                    <?php $banner_twitter = synved_option_get('options', 'banner_twitter');?>
                    <?php $banner_youtube = synved_option_get('options', 'banner_youtube');?>
                    <?php $banner_soundcloud = synved_option_get('options', 'banner_soundcloud');?>
                    <?php $banner_itunes = synved_option_get('options', 'banner_itunes');?>
                    <?php $banner_beatport = synved_option_get('options', 'banner_beatport');?>
                    <?php $banner_vk = synved_option_get('options', 'banner_vk');?>
                    <?php if($banner_facebook || $banner_twitter || $banner_youtube || $banner_soundcloud || $banner_itunes || $banner_beatport || $banner_vk){?>
                        <ul class="social-links-footer">
                            <?php if($banner_vk){?><li id="vk"><a href="<?php echo $banner_vk;?>"></a></li><?php } ?>
                            <?php if($banner_facebook){?><li id="facebook"><a href="<?php echo $banner_facebook;?>"></a></li><?php } ?>
                            <?php if($banner_twitter){?><li id="twitter"><a href="<?php echo $banner_twitter;?>"></a></li><?php } ?>
                            <?php if($banner_youtube){?><li id="youtube"><a href="<?php echo $banner_youtube;?>"></a></li><?php } ?>
                            <?php if($banner_soundcloud){?><li id="soundcloud"><a href="<?php echo $banner_soundcloud;?>"></a></li><?php } ?>
                            <?php if($banner_itunes){?><li id="itunes"><a href="<?php echo $banner_itunes;?>"></a></li><?php } ?>
                            <?php if($banner_beatport){?><li id="beatport"><a href="<?php echo $banner_beatport;?>"></a></li><?php } ?>
                        </ul>
                    <?php } ?>
                </div>
			</div>
			<div id="menu"><div id="wrapper"><?php wp_nav_menu(array('menu' => 'menu')); ?></div><div class="fix"></div></div>
				<?php if ( is_page_template('main.php') ) { ?>
					<div id="video">
						<div id="wrapper">
							<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
							<?php endwhile; // end of the loop. ?>
							<?php wp_reset_query(); ?>
							<div class="fix"></div>
						</div>
					</div>
				<?php } else { ?>
				<?php } ?>
			</div>

		<div id="wrapper-content">
		<?//php } ?>

