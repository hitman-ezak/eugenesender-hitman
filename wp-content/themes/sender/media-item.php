<?php
/**
 * Template Name: Media Item Template
 *
 * A custom media item page template.
 *
*/

get_header(); ?>
		<div class="content item">	
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<?php
					$parentstop = get_post_ancestors( $post->ID );
					$idparent = ($parentstop) ? $parentstop[count($parentstop)-1]: $post->ID;
				?>
						<div class="page-title">
							<h1><font style="color:#666666;"><?php echo get_the_title($idparent);?></font> &gt; <?php echo get_the_title($post->post_parent);?></h1></div>
							<div class="media-views"><?php echo do_shortcode('[post_view]'); ?></div>
							<div class="media-date">
								<?php if ( get_post_meta($post->post_parent, 'icon_parent_tag', true) ) : ?>
								<?php $image_icon = wp_get_attachment_image_src (get_post_meta($post->post_parent, 'icon_parent_tag', true)); ?>
									<img src="<?php echo $image_icon[0]; ?>" />
								<?php endif; ?>
								<?php the_time('j F Y'); ?><?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?>
							</div>
						
							<?php if(get_field('music_or_video'))
										{
											echo '<div class="item_in_content">'. get_field('music_or_video');
										}
									else {
										echo '<div class="item_in_content">';
									}
							?>
								
								<h3 style="font-size:36px;"><strong><?php the_title(); ?></strong></h3>
								<?php the_content(); ?>
						
							</div>
							
							<div class="fix"></div>

					<div class="blog-comment-tabber"><?php comments_template( '', true ); ?></div>
					<div class="fix"></div>
				<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>