<?php 
/**
 * Template Name: Landing Page Template
 *
 * A custom Landing Page template.
 *
*/

get_header(); ?>
		<div class="content">
				<ul id="mycarousel" class="jcarousel-skin-tango">
				<?php
			$paged = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
			$title = get_the_title($post->ID);
			$args= array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'paged' => $paged,
			);
			query_posts($args);?>
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<li>
					<a class="entry-title" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><h1><?php the_title(); ?></h1></a>
					<div class="posted">
						<div class="date"><?php the_time('j F Y'); ?></div>
						<div class="comments"><?php comments_number(); ?></div>
					</div>
					<div class="entry-content">
						<div style="text-align:center"><?php the_post_thumbnail('large'); ?></div>
						<?php the_content(); ?>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</li>
				<?php endwhile; // end of the loop. ?>
				<?php wp_reset_query(); ?>
				</ul>
				<div class="blog-comment-tabber"><?php comments_template( '', true ); ?></div>
<?php get_footer(); ?>