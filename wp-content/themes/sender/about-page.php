<?php
/**
 * Template Name: About Template
 *
 * About page.
 *
*/

get_header(); ?>
		<div class="content">
			<div class="right">
			<?php if ( qtrans_getLanguage() == 'en' ) {
				if(get_field('partner1-eng'))
					{
						echo '<div class="partner">' . get_field('partner1-eng') . '</div>';
					}
				if(get_field('partner2-eng'))
					{
						echo '<div class="partner">' . get_field('partner2-eng') . '</div>';
					}
				
				if(get_field('partner3-eng'))
					{
						echo '<div class="partner">' . get_field('partner3-eng') . '</div>';
					}
				
				if(get_field('partner4-eng'))
					{
						echo '<div class="partner">' . get_field('partner4-eng') . '</div>';
					}
				
				if(get_field('partner5-eng'))
					{
						echo '<div class="partner">' . get_field('partner5-eng') . '</div>';
					}
				
			} else {
				if(get_field('partner1'))
					{
						echo '<div class="partner">' . get_field('partner1') . '</div>';
					}
				if(get_field('partner2'))
					{
						echo '<div class="partner">' . get_field('partner2') . '</div>';
					}
				
				if(get_field('partner3'))
					{
						echo '<div class="partner">' . get_field('partner3') . '</div>';
					}
				
				if(get_field('partner4'))
					{
						echo '<div class="partner">' . get_field('partner4') . '</div>';
					}
				
				if(get_field('partner5'))
					{
						echo '<div class="partner">' . get_field('partner5') . '</div>';
					}
			}
			?>
			</div>
			
			<div class="left">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
					<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
				<?php endwhile; // end of the loop. ?>
				
				<?php if ( qtrans_getLanguage() == 'en' ) {
					if(get_field('booking_eng'))
						{
							echo '<div class="booking"><h2>Booking</h2>' . get_field('booking_eng') . '<div class="fix"></div></div>';
						}
						
					if(get_field('promo_eng'))
						{
							echo '<div class="promo"><h2>Promo</h2>' . get_field('promo_eng') . '<div class="fix"></div></div>';
						}
						
					if(get_field('download1_name_eng'))
						{
							echo '<h2 class="h2-download">Download</h2><div class="download"><a href="' . get_field('download1_eng') . '" target="_blank">' . get_field('download1_name_eng') . '</a></div>';
						}
					if(get_field('download2_name_eng'))
						{
							echo '<div class="download"><a href="' . get_field('download2_eng') . '" target="_blank">' . get_field('download2_name_eng') . '</a></div>';
						}
					if(get_field('download3_name_eng'))
						{
							echo '<div class="download"><a href="' . get_field('download3_eng') . '" target="_blank">' . get_field('download3_name_eng') . '</a></div>';
						}
						
				} else {
					if(get_field('booking'))
						{
							echo '<div class="booking"><h2>Booking</h2>' . get_field('booking') . '<div class="fix"></div></div>';
						}
						
					if(get_field('promo'))
						{
							echo '<div class="promo"><h2>Promo</h2>' . get_field('promo') . '<div class="fix"></div></div>';
						}
					
					if(get_field('download1_name'))
						{
							echo '<h2 class="h2-download">Скачать</h2><div class="download"><a href="' . get_field('download1') . '" target="_blank">' . get_field('download1_name') . '</a></div>';
						}
					if(get_field('download2_name'))
						{
							echo '<div class="download"><a href="' . get_field('download2') . '" target="_blank">' . get_field('download2_name') . '</a></div>';
						}
					if(get_field('download3_name'))
						{
							echo '<div class="download"><a href="' . get_field('download3') . '" target="_blank">' . get_field('download3_name') . '</a></div>';
						}
						
				}
				?>
			</div>	
<?php get_footer(); ?>