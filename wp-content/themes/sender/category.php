<?php get_header(); ?>
		<div class="content">
		<h1><?php single_cat_title(); ?></h1>
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<div class="catalog-item">
								<div class="catalog-thumb"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a></div>
								<div class="catalog-content">
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>					
									<div class="catalog-text"><?php the_advanced_excerpt('length=30&exclude_tags=img&allowed_tags=p,strong'); ?></div>
								</div>
							</div>
				<?php endwhile; // end of the loop. ?>
				<div class="fix"></div>
				<?php wp_simple_pagination(); ?> 

<?php get_footer(); ?>