<?php get_header(); ?>
		<div class="content single">
			<div class="right">
				<div class="page-title">
					<h2><?php if ( qtrans_getLanguage() == 'en' ) { ?>
						Related News
						<?php } else { ?>
						Похожие публикации
					<?php } ?></h2>
				</div>
				
				<?php
					//for use in the loop, list 5 post titles related to first tag on current post
					$tags = wp_get_post_tags($post->ID);
					if ($tags) {
					$first_tag = $tags[0]->term_id;
					$args=array(
					'tag__in' => array($first_tag),
					'post__not_in' => array($post->ID),
					'showposts'=>3,
					'caller_get_posts'=>1
					);
					$my_query = new WP_Query($args);
					if( $my_query->have_posts() ) {
					while ($my_query->have_posts()) : $my_query->the_post(); ?>
					<div class="blog-item-single">
						<div class="blog-date"><?php the_time('j F Y'); ?><?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?></div>
						<?php if(get_field('post_video_thumb'))
								{
									echo '<div class="blog-thumb-video">' . get_field('post_video_thumb') . '</div>';
								}
				else { ?>
				<div class="blog-img"><a class="blog-thumb" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_post_thumbnail('thumbnail'); ?></a></div>
				<?php } ?>
						<a class="blog-title" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><h1><strong><?php the_title(); ?></strong></h1></a>
						<div class="blog-content">
							<?php the_excerpt(); ?>
						</div><!-- .entry-content -->
						<div class="blog-comments"><span><?php comments_number('0', '1', '%'); ?></span></div>
					</div>
					<?php
					endwhile;
					}
					wp_reset_query();
					}
				?>
				<div class="fix" style="margin:40px 0;"></div>
					<div class="all-archive">
						<a href="/blog">
							<?php if ( qtrans_getLanguage() == 'en' ) { ?>
								All Publications
								<?php } else { ?>
								Все публикации
							<?php } ?>
						</a>
					</div>
			</div>
			<div class="left">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<?php echo do_shortcode('[gallery size="medium" link="file"]'); ?>
					<div class="blog-date"><?php the_time('j F Y'); ?><?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?></div>
					<h1 class="blog-title single"><strong><?php the_title(); ?></strong></h1>

					<div class="blog-content">
						<?php the_content(); ?>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
					
					<div class="blog-tags">
						<div class="tags-cloud"><?php the_tags(''); ?></div>
						<div class="fix"></div>
					</div>

				<?php endwhile; // end of the loop. ?>
				<div class="blog-comment-tabber"><?php comments_template( '', true ); ?></div>
				<div class="fix"></div>
				</div>
				<div class="fix"></div>
				<div class="post-archive"><div class="all-archive"><a href="/blog"><?php if ( qtrans_getLanguage() == 'en' ) { ?>
						All Archive
						<?php } else { ?>
						Весь архив
					<?php } ?></a></div>
					<div class="post-archive-item"><?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 6 ) ); ?></div>
				</div>	
<?php get_footer(); ?>