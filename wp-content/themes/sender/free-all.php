<?php
/**
 * Template Name: All Free Produts Template
 *
 * A custom All Free Produts page template.
 *
*/

get_header(); ?>
<div class="content shop">
					<?php
						$postIDs = array();
						$pageChildren = get_pages('include=' . $gen3_ids);
	
						if ( $pageChildren ) {
						  foreach ( $pageChildren as $pageChild ) {
							$postIDs[] = $pageChild->ID;
						  }
						  $args = array(
							'post_type' => 'page',
							'post__in' => $postIDs,
							'orderby'  => 'post_date',
							'order' => 'DESC',	
							'meta_key' => 'slider_featured',
							'meta_value' => 'yes',				
							);
						  query_posts($args);
										  
							if ( have_posts() ) :?>
							<div class="slider-shop">
							<div id="featured-navi">
								<div id="nav-featured" class="bubbles-nav"></div>
									<div id="featured">
										<?php while ( have_posts() ) : the_post();?>
											<div class="item">
													<div class="shop-slider-info">
														<div class="slider-price">
															<?php if ( qtrans_getLanguage() == 'en' ) {
								
															if(get_field('download_number_eng'))
															{
																echo '<div class="number-downloads">' . do_shortcode(get_field('download_number')) . '</div>';
															}
															} else {
																if(get_field('download_number'))
																{
																	echo '<div class="number-downloads">' . do_shortcode(get_field('download_number')) . '</div>';
																}
															}?>
														</div>
														<div class="parent-tag">
															<?php if ( get_post_meta($post->post_parent, 'icon_parent_tag', true) ) : ?>
																<?php $image_icon = wp_get_attachment_image_src (get_post_meta($post->post_parent, 'icon_parent_tag', true)); ?>
																<img src="<?php echo $image_icon[0]; ?>" />
															<?php endif; ?>
															<?php echo get_the_title($post->post_parent); ?>									
														</div>
														
														<a href="<?php the_permalink(); ?>">
															<h3><?php the_title(); ?></h3>
														</a>
														<?php the_excerpt(); ?>
														</div>
														<div class="slider-buy-button">
															<?php if ( qtrans_getLanguage() == 'en' ) {
												
												global $user_ID, $user_identity, $user_level ?>			
												<?php if ( $user_ID ) : 
												if(get_field('download_button_free_eng'))
												{
													echo '<div class="buy-button"><a href="' . do_shortcode(get_field('link_for_download')) . '">' . get_field('download_button_free_eng') . '</a></div>';
												}	?>				
												<?php else : ?>
												
												<?php if(!is_user_logged_in()) { 
													if(get_field('download_button_free_eng'))
													{
														echo '<div class="buy-button"><a href="/wp-login.php?action=register" class="simplemodal-register">' . get_field('download_button_free_eng') . '</a></div>';
													}
												} ?>
												
												<?php endif;
												
												} else {
													global $user_ID, $user_identity, $user_level ?>			
												<?php if ( $user_ID ) : 
												if(get_field('download_button_free'))
												{
													echo '<div class="buy-button"><a href="' . do_shortcode(get_field('link_for_download')) . '">' . get_field('download_button_free') . '</a></div>';
												}	?>				
												<?php else : ?>
												
												<?php if(!is_user_logged_in()) { 
													if(get_field('download_button_free'))
													{
														echo '<div class="buy-button"><a href="/wp-login.php?action=register" class="simplemodal-register">' . get_field('download_button_free') . '</a></div>';
													}
												} ?>
												
												<?php endif;
												}?>
														</div>
													<div class="shop-slider-image"><?php echo '<img src="' .get_field('image_slider'). '" />'; ?></div>
										</div>
										
								<?php endwhile;?>
								</div>
							</div>
						</div>
						
						<?php
							else :
								echo wpautop( '' );
							endif;
							?>												
				<?php }?>
				<?php wp_reset_query(); ?>
			
	<div class="shop-tags">
		<!-- Start Code -->
			<?php
				$postIDs = array();
				$pageChildren = get_pages(
				array (
				'child_of' => $post->ID,
				'parent' => $post->ID,
				)
			);
	
			if ( $pageChildren ) {
				foreach ( $pageChildren as $pageChild ) {
				$postIDs[] = $pageChild->ID;
			}
			$args = array(
				'post_type' => 'page',
				'post__in' => $postIDs,
				'orderby'  => 'post_date',
				'order' => 'DESC',
				'posts_per_page' => 10,							
			);
			query_posts($args);
				if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="shop-tag">
						<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</div>					  
				<?php endwhile; ?>
				<?php endif; ?>
				<div class="fix"></div>
			<?php }?>
		<?php wp_reset_query(); ?>
		<!-- End Code -->	
	</div>	
			<!-- Start Code -->
			
					<ul>
					<?php
						$postIDs = array();
						$pageChildren = get_pages('include=' . $gen3_ids);
	
						if ( $pageChildren ) {
						  foreach ( $pageChildren as $pageChild ) {
							$postIDs[] = $pageChild->ID;
						  }
						  $paged = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
						  $args = array(
							'post_type' => 'page',
							'paged' => $paged,
							'post__in' => $postIDs,
							'orderby'  => 'post_date',
							'order' => 'DESC',						
							);
						  query_posts($args);
						
						  if (have_posts()) : while (have_posts()) : the_post(); ?>
								<li class="shop-content">
									<div class="parent-tag">
											<?php if ( get_post_meta($post->post_parent, 'icon_parent_tag', true) ) : ?>
												<?php $image_icon = wp_get_attachment_image_src (get_post_meta($post->post_parent, 'icon_parent_tag', true)); ?>
												<img src="<?php echo $image_icon[0]; ?>" />
											<?php endif; ?>
											<?php echo get_the_title($post->post_parent); ?> 
											<?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?>										
									</div>
									<div class="shop-thumb">
										<div class="bottom">
												<?php if ( qtrans_getLanguage() == 'en' ) {
												
												global $user_ID, $user_identity, $user_level ?>			
												<?php if ( $user_ID ) : 
												if(get_field('download_button_free_eng'))
												{
													echo '<div class="buy-button"><a href="' . do_shortcode(get_field('link_for_download')) . '">' . get_field('download_button_free_eng') . '</a></div>';
												}	?>				
												<?php else : ?>
												
												<?php if(!is_user_logged_in()) { 
													if(get_field('download_button_free_eng'))
													{
														echo '<div class="buy-button"><a href="/wp-login.php?action=register" class="simplemodal-register">' . get_field('download_button_free_eng') . '</a></div>';
													}
												} ?>
												
												<?php endif;
												
												} else {
													global $user_ID, $user_identity, $user_level ?>			
												<?php if ( $user_ID ) : 
												if(get_field('download_button_free'))
												{
													echo '<div class="buy-button"><a href="' . do_shortcode(get_field('link_for_download')) . '">' . get_field('download_button_free') . '</a></div>';
												}	?>				
												<?php else : ?>
												
												<?php if(!is_user_logged_in()) { 
													if(get_field('download_button_free'))
													{
														echo '<div class="buy-button"><a href="/wp-login.php?action=register" class="simplemodal-register">' . get_field('download_button_free') . '</a></div>';
													}
												} ?>
												
												<?php endif;
												}?>
										</div>
										<div class="top"><?php the_post_thumbnail('thumbnail'); ?></div>
									</div>
									<a href="<?php the_permalink(); ?>">
										<h3><?php the_title(); ?></h3>
									</a>
									<div class="fix"></div>

									<?php if ( qtrans_getLanguage() == 'en' ) {
								
										if(get_field('download_number'))
										{
											echo '<div class="number-downloads-items"><span class="number-downloads">' . do_shortcode(get_field('download_number')) . '</span></div>';
										}
										} else {
											if(get_field('download_number'))
											{
												echo '<div class="number-downloads-items"><span class="number-downloads">' . do_shortcode(get_field('download_number')) . '</span></div>';
											}
										}?>
								</li>					  
						  <?php endwhile; ?>
						
						<?php endif; ?>
						<div class="fix"></div>
						<?php wp_simple_pagination(); ?>
						
						<?php }?>
						<?php wp_reset_query(); ?>
						</ul>
					<!-- End Code -->
<?php get_footer(); ?>