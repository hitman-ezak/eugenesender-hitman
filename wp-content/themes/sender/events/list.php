<?php

/**

 * Grid view template.  This file loads the TEC month view, specifically the 

 * month view navigation.  The actual rendering if the calendar happens in the 

 * table.php template.

 *

 * You can customize this view by putting a replacement file of the same name (gridview.php) in the events/ directory of your theme.

 */



// Don't load directly

if ( !defined('ABSPATH') ) { die('-1'); }

$tribe_ecp = TribeEvents::instance();

?>	

	<div id="tribe-events-content" class="grid" data-title="<?php wp_title(); ?>">

		<div id='tribe-events-calendar-header' class="clearfix">

			<div style="float:right">
				<span class='tribe-events-month-nav'>
					<span class='tribe-events-prev-month'>
						<a href='<?php echo tribe_get_previous_month_link(); ?>' class="tribe-pjax">
						&#x2190; <?php echo tribe_get_previous_month_text(); ?>
						</a>
					</span>

					<span class='tribe-events-next-month'>
						<a href='<?php echo tribe_get_next_month_link(); ?>' class="tribe-pjax">
						<?php echo tribe_get_next_month_text(); ?> &#x2192; 
						</a>
				   		<img src="<?php echo esc_url( admin_url( 'images/wpspin_light.gif' ) ); ?>" class="ajax-loading" id="ajax-loading" alt="" style='display: none'/>
					</span>
				</span>
			</div>
			
			<?php tribe_month_year_dropdowns( "tribe-events-" ); ?>

		</div> 
		<div class="fix"></div>

		<?php tribe_calendar_grid(); // See the views/table.php template for customization ?>

	</div>