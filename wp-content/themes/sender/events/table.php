<?php

/**

* This file outputs the actual days of the month in the TEC calendar view

*

* You can customize this view by putting a replacement file of the same name (table.php) in the events/ directory of your theme.

*/



// Don't load directly

if ( !defined('ABSPATH') ) { die('-1'); }

$tribe_ecp = TribeEvents::instance();

// in an events cat

if ( is_tax( $tribe_ecp->get_event_taxonomy() ) ) {

	$cat = get_term_by( 'slug', get_query_var('term'), $tribe_ecp->get_event_taxonomy() );

	$eventCat = (int) $cat->term_id;

	$eventPosts = tribe_get_events( array( 'eventCat' => $eventCat, 'time_order' => 'ASC', 'eventDisplay'=>'month' ) );

} // not in a cat

else {

	$eventPosts = tribe_get_events(array( 'eventDisplay'=>'month' ));

}

$daysInMonth = isset($date) ? date("t", $date) : date("t");

$startOfWeek = get_option( 'start_of_week', 0 );

list( $year, $month ) = split( '-', $tribe_ecp->date );

$date = mktime(12, 0, 0, $month, 1, $year); // 1st day of month as unix stamp

$rawOffset = date("w", $date) - $startOfWeek;

$offset = ( $rawOffset < 0 ) ? $rawOffset + 7 : $rawOffset; // month begins on day x

$rows = 1;



$monthView = tribe_sort_by_month( $eventPosts, $tribe_ecp->date );



?>

<table class="tribe-events-calendar" id="big">

	<thead>

			<tr>

				<?php

				for( $n = $startOfWeek; $n < count($tribe_ecp->daysOfWeek) + $startOfWeek; $n++ ) {

					$dayOfWeek = ( $n >= 7 ) ? $n - 7 : $n;

					

					echo '<th id="tribe-events-' . strtolower($tribe_ecp->daysOfWeek[$dayOfWeek]) . '" abbr="' . $tribe_ecp->daysOfWeek[$dayOfWeek] . '">' . $tribe_ecp->daysOfWeekShort[$dayOfWeek] . '</th>';

				}

				?>

			</tr>

	</thead>



	<tbody>

		<tr>

		<?php

			// skip last month

			for( $i = 1; $i <= $offset; $i++ ){ 

				echo "<td class='tribe-events-othermonth'></td>";

			}

			// output this month

         $days_in_month = date("t", intval($date));

			for( $day = 1; $day <= $days_in_month; $day++ ) {

			    if( ($day + $offset - 1) % 7 == 0 && $day != 1) {

			        echo "</tr>\n\t<tr>";

			        $rows++;

			    }

			

				// Var'ng up days, months and years

				$current_day = date_i18n( 'd' );

				$current_month = date_i18n( 'm' );

				$current_year = date_i18n( 'Y' );

            $date = "$year-$month-$day";

				

				if ( $current_month == $month && $current_year == $year) {

					// Past, Present, Future class

					if ($current_day == $day ) {

						$ppf = ' tribe-events-present';

					} elseif ($current_day > $day) {

						$ppf = ' tribe-events-past';

					} elseif ($current_day < $day) {

						$ppf = ' tribe-events-future';

					}

				} elseif ( $current_month > $month && $current_year == $year || $current_year > $year ) {

					$ppf = ' tribe-events-past';

				} elseif ( $current_month < $month && $current_year == $year || $current_year < $year ) {

					$ppf = ' tribe-events-future';

				} else { $ppf = false; }

				

			    echo "<td class='tribe-events-thismonth" . $ppf . "'>" . display_day_title( $day, $monthView, $date ) . "\n";

				echo display_day( $day, $monthView );

				echo "</td>";

			}

			// skip next month

			while( ($day + $offset) <= $rows * 7)

			{

			    echo "<td class='tribe-events-othermonth'></td>";

			    $day++;

			}

		?>

		</tr>

	</tbody>

</table>

<?php



function display_day_title( $day, $monthView, $date ) {

	$return = "<div class='daynum tribe-events-event' id='daynum_$day'>";

	if( function_exists('tribe_get_linked_day') && count( $monthView[$day] ) > 0 ) {

		$return .= tribe_get_linked_day($date, $day); // premium

	} else {

    	$return .= $day;

	}

	$return .= "<div id='tooltip_day_$day' class='tribe-events-tooltip' style='display:none;'>";

	for( $i = 0; $i < count( $monthView[$day] ); $i++ ) {

		$post = $monthView[$day][$i];

		setup_postdata( $post );

		$return .= '<h5 class="tribe-events-event-title">' . get_the_title() . '</h5>';

	}

	$return .= '<span class="tribe-events-arrow"></span>';

	$return .= '</div>';



	$return .= "</div>";

	return $return;

}



function display_day( $day, $monthView ) {

	global $post;

	$output = '';

	$posts_per_page = tribe_get_option( 'postsPerPage', 10 );

	for ( $i = 0; $i < count( $monthView[$day] ); $i++ ) {

		$post = $monthView[$day][$i];

		setup_postdata( $post );

		$eventId	= $post->ID.'-'.$day;

		$start		= tribe_get_start_date( $post->ID, false, 'U' );

		$end		= tribe_get_end_date( $post->ID, false, 'U' );

		$cost		= tribe_get_cost( $post->ID );

		?>

		<div id="event_<?php echo $eventId; ?>" <?php post_class('tribe-events-event tribe-events-real-event') ?>>

			<a href="<?php tribe_event_link(); ?>"><?php the_title(); ?></a>

			<div id="tooltip_<?php echo $eventId; ?>" class="tribe-events-tooltip" style="display:none;">

				<h5 class="tribe-events-event-title"><?php the_title();?></h5>

				<div class="tribe-events-event-body">

					<div class="tribe-events-event-date">

						<?php if ( !empty( $start ) )	echo date_i18n( get_option('date_format', 'F j, Y'), $start);

						if ( !tribe_get_event_meta($post->ID, '_EventAllDay', true) )

							echo ' ' . date_i18n( get_option('time_format', 'g:i a'), $start); ?>

						<?php if ( !empty( $end )  && $start !== $end ) {

							if ( date_i18n( 'Y-m-d', $start ) == date_i18n( 'Y-m-d', $end ) ) {

								$time_format = get_option( 'time_format', 'g:i a' );

								if ( !tribe_get_event_meta($post->ID, '_EventAllDay', true) )

									echo " – " . date_i18n( $time_format, $end );

							} else {

								echo " – " . date_i18n( get_option('date_format', 'F j, Y'), $end);

								if ( !tribe_get_event_meta($post->ID, '_EventAllDay', true) )

								 	echo ' ' . date_i18n( get_option('time_format', 'g:i a'), $end) . '<br />';

							}

						} ?>

					</div>

					<?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { ?>

						<div class="tribe-events-event-thumb"><?php the_post_thumbnail( array(75,75));?></div>

					<?php } ?>

					<?php the_excerpt(); ?>

				</div>

				<span class="tribe-events-arrow"></span>

			</div>

		</div>

		<?php

		if( $i < count( $monthView[$day] ) - 1 ) { 

			echo "<hr />";

		}

	}

}

?>
</div>
<?php wp_reset_query();?>
<div class="tabberlive style1">
	<ul class="tabbernav event">
		<li id="kv_s" onclick="showVK();CngClass(this);"><a class="vk_recount" id="submit" style="white-space:nowrap">
			<?php if ( qtrans_getLanguage() == 'en' ) {	?>							
				Future
					<?php } else {?>	
				Будет	
			<?php }?>		
		</a></li>
		<li id="fb_s" onclick="showFB();CngClass(this);"><a class="vk_recount" id="submit" style="white-space:nowrap">
			<?php if ( qtrans_getLanguage() == 'en' ) {	?>							
				Past
					<?php } else {?>	
				Было	
			<?php }?>
		</a></li>
	</ul>
</div>
<div id="vkapi" class="event">
<div id="tribe-events-content">
<div class="fix"></div>
<?php global $post;

	 	$paged = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
		$args = array(
			'post_type' => 'tribe_events',
			'paged' => $paged,
			'eventDisplay' => 'upcoming',
			'orderby'  => 'post_date',
			'order' => 'DESC',
		);
		query_posts($args);
		if (have_posts()) : while (have_posts()) : the_post();?>
		
		<div class="event-item">
		<div class="event-title-top">	
			<?php if(get_field('category_icon'))
				{
					echo '<div class="category_icon"><img src="' . get_field('category_icon') . '"/></div>';
				}
			?>
			<div class="event_day"><?php echo tribe_get_start_date( null, false ); ?></div>
			<?php if ( qtrans_getLanguage() == 'en' ) {								
				if(get_field('event_cat_en'))
					{
						echo '<div class="event-cat">/ ' . do_shortcode(get_field('event_cat_en')) . '</div>';
					}
					} else {
						if(get_field('event_cat_ru'))
							{
								echo '<div class="event-cat">/ ' . do_shortcode(get_field('event_cat_ru')) . '</div>';
							}
					}?>
		</div>
	
		<?php if(has_post_thumbnail($post->ID, 'thumbnail')):
		$image_id = get_post_thumbnail_id($post->ID);
		$image_url = wp_get_attachment_image_src($image_id, 'thumbnail', true); ?>
		<a href="<?php echo get_permalink($post->ID) ?>" class="post-thumb"><img src="<?php echo $image_url[0]; ?>" alt="<?php echo $post->post_title ?>" /></a>
		<?php endif; ?>
		
		<div class="event-title-link">
			<a href="<?php echo get_permalink($post->ID) ?>">
				<h3><strong><?php echo $post->post_title; ?></strong></h3>
			</a>
		</div>
		
		<div class="event-adress">
			<?php _e( tribe_get_venue( get_the_ID() )); ?><?php _e(',&nbsp;'. tribe_get_address( get_the_ID() )); ?><?php _e(',&nbsp;' .tribe_get_city( get_the_ID() )); ?><?php _e(',&nbsp;' .tribe_get_country( get_the_ID() )); ?>
		</div>
		
		<div class="fix"></div>
	
		<?php the_excerpt(); ?>
		
		<div class="links-events">
			<?php if ( qtrans_getLanguage() == 'en' ) {								
				if(get_field('buy_ticket_en'))
					{
						echo '<div class="buy_ticket"><a href="' . do_shortcode(get_field('buy_ticket_event')) . '">' . do_shortcode(get_field('buy_ticket_en')) . '</a></div>';
					}
					} else {
						if(get_field('buy_ticket_ru'))
							{
								echo '<div class="buy_ticket"><a href="' . do_shortcode(get_field('buy_ticket_event')) . '">' . do_shortcode(get_field('buy_ticket_ru')) . '</a></div>';
							}
			}?>
			
			<?php if(get_field('vk_event'))
					{
						echo '<div class="vk_event"><a href="' . do_shortcode(get_field('vk_event')) . '"><img src="/wp-content/themes/sender/images/vk-icon-event.png" /></a></div>';
					}
			?>
			
			<?php if(get_field('fb_event'))
					{
						echo '<div class="fb_event"><a href="' . do_shortcode(get_field('fb_event')) . '"><img src="/wp-content/themes/sender/images/facebook-icon-event.png" /></a></div>';
					}
			?>
		</div>
		<div class="fix"></div>
		<div class="blog-comments"><span><?php comments_number('0', '1', '%'); ?></span></div>
	</div>	
<?php endwhile;?>
<div class="fix"></div>
<?php if ( function_exists('wp_simple_pagination') ) wp_simple_pagination( array( 'query' => $upcoming ) ); // tell it which query we are paginating
	endif;
	wp_reset_query(); // important to reset the query
?>
</div>
</div>
	
<div class="fb-comments event" style="display:none">
<div class="fix"></div>

<?php global $post;

	 	$paged = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
		$args = array(
			'post_type' => 'tribe_events',
			'paged' => $paged,
			'eventDisplay' => 'past',
			'orderby'  => 'post_date',
			'order' => 'DESC',
		);
		query_posts($args);
		if (have_posts()) : while (have_posts()) : the_post();?>

		<div class="event-item">
		<div class="event-title-top">	
			<?php if(get_field('category_icon'))
				{
					echo '<div class="category_icon"><img src="' . get_field('category_icon') . '"/></div>';
				}
			?>
			<div class="event_day"><?php echo tribe_get_start_date( null, false ); ?></div>
			<?php if ( qtrans_getLanguage() == 'en' ) {								
				if(get_field('event_cat_en'))
					{
						echo '<div class="event-cat">/ ' . do_shortcode(get_field('event_cat_en')) . '</div>';
					}
					} else {
						if(get_field('event_cat_ru'))
							{
								echo '<div class="event-cat">/ ' . do_shortcode(get_field('event_cat_ru')) . '</div>';
							}
					}?>
		</div>
	
		<?php if(has_post_thumbnail($post->ID, 'thumbnail')):
		$image_id = get_post_thumbnail_id($post->ID);
		$image_url = wp_get_attachment_image_src($image_id, 'thumbnail', true); ?>
		<a href="<?php echo get_permalink($post->ID) ?>" class="post-thumb"><img src="<?php echo $image_url[0]; ?>" alt="<?php echo $post->post_title ?>" /></a>
		<?php endif; ?>
		
		<div class="event-title-link">
			<a href="<?php echo get_permalink($post->ID) ?>">
				<h3><strong><?php echo $post->post_title; ?></strong></h3>
			</a>
		</div>
		
		<div class="event-adress">
				<?php _e( tribe_get_venue( get_the_ID() )); ?><?php _e(',&nbsp;'. tribe_get_address( get_the_ID() )); ?><?php _e(',&nbsp;' .tribe_get_city( get_the_ID() )); ?><?php _e(',&nbsp;' .tribe_get_country( get_the_ID() )); ?>
		</div>
		
		<div class="fix"></div>
	
		<?php the_excerpt(); ?>
		
		<div class="links-events">
			<?php if ( qtrans_getLanguage() == 'en' ) {								
				if(get_field('photo_events'))
					{
						echo '<div class="photo_events"><a href="' . do_shortcode(get_field('photo_events')) . '">Photo Gallery</a></div>';
					}
					} else {
						if(get_field('photo_events'))
							{
								echo '<div class="photo_events"><a href="' . do_shortcode(get_field('photo_events')) . '">Фотоотчет</a></div>';
							}
			}?>
			
			<?php if(get_field('vk_event'))
					{
						echo '<div class="vk_event"><a href="' . do_shortcode(get_field('vk_event')) . '"><img src="/wp-content/themes/sender/images/vk-icon-event.png" /></a></div>';
					}
			?>
			
			<?php if(get_field('fb_event'))
					{
						echo '<div class="fb_event"><a href="' . do_shortcode(get_field('fb_event')) . '"><img src="/wp-content/themes/sender/images/facebook-icon-event.png" /></a></div>';
					}
			?>
		</div>
		<div class="fix"></div>
		<div class="blog-comments"><span><?php comments_number('0', '1', '%'); ?></span></div>
	</div>
<?php endwhile;?>
<div class="fix"></div>
<?php if ( function_exists('wp_simple_pagination') ) wp_simple_pagination( array( 'query' => $upcoming ) ); // tell it which query we are paginating
	endif;
	wp_reset_query(); // important to reset the query
?>