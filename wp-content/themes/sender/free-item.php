<?php
/**
 * Template Name: Free Item Template
 *
 * A custom free item page template.
 *
*/

get_header(); ?>
		<div class="content shop item">	
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
						<?php
									$postIDs = array();
									
									$parentstop = get_post_ancestors( $post->ID );
									$idparent = ($parentstop) ? $parentstop[count($parentstop)-1]: $post->ID;
									
									$pageChildren = get_pages(
									array (
									'child_of' => $idparent,
									'parent' => $idparent,
									)
						);?>
						<div class="page-title">
							<h1><font style="color:#666666;"><?php echo get_the_title($idparent);?></font> &gt; <?php echo get_the_title($post->post_parent);?></h1></div>
							<div class="shop-tags">
							<!-- Start Code -->
							<?php
									if ( $pageChildren ) {
									  foreach ( $pageChildren as $pageChild ) {
										$postIDs[] = $pageChild->ID;
									  }
									  $args = array(
										'post_type' => 'page',
										'post__in' => $postIDs,
										'orderby'  => 'post_date',
										'order' => 'DESC',						
										);
									  query_posts($args);
									if (have_posts()) : while (have_posts()) : the_post(); ?>
										<div class="shop-tag">
											<a href="<?php the_permalink(); ?>">
												<?php the_title(); ?>
											</a>
										</div>					  
									<?php endwhile; ?>
									<?php endif; ?>
									<div class="fix"></div>
								<?php }?>
							<?php wp_reset_query(); ?>
							<!-- End Code -->	
						</div>
							<?php if(get_field('img_or_video'))
										{
											echo '<div class="page-in-left">'. get_field('img_or_video') .'</div><div class="page-in-right">';
										}
									else {
										echo '<div class="item_in_content">';
									}
							?>
								<?php if ( qtrans_getLanguage() == 'en' ) {
								
									if(get_field('download_number'))
									{
										echo '<div class="number-downloads">' . do_shortcode(get_field('download_number')) . '</div>';
									}
									} else {
										if(get_field('download_number'))
										{
											echo '<div class="number-downloads">' . do_shortcode(get_field('download_number')) . '</div>';
										}
									}?>
									
								<div class="parent-tag">
											<?php if ( get_post_meta($post->post_parent, 'icon_parent_tag', true) ) : ?>
												<?php $image_icon = wp_get_attachment_image_src (get_post_meta($post->post_parent, 'icon_parent_tag', true)); ?>
												<img src="<?php echo $image_icon[0]; ?>" />
											<?php endif; ?>
											<?php echo get_the_title($post->post_parent); ?> 
											<?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?>										
								</div>
								
								<h3><strong><?php the_title(); ?></strong></h3>
								<?php the_content(); ?>

								<?php if ( qtrans_getLanguage() == 'en' ) {
												
												global $user_ID, $user_identity, $user_level ?>			
												<?php if ( $user_ID ) : 
												if(get_field('download_button_free_eng'))
												{
													echo '<div class="buy-button"><a href="' . do_shortcode(get_field('link_for_download')) . '">' . get_field('download_button_free_eng') . '</a></div>';
												}	?>				
												<?php else : ?>
												
												<?php if(!is_user_logged_in()) { 
													if(get_field('download_button_free_eng'))
													{
														echo '<div class="buy-button"><a href="/wp-login.php?action=register" class="simplemodal-register">' . get_field('download_button_free_eng') . '</a></div>';
													}
												} ?>
												
												<?php endif;
												
												} else {
													global $user_ID, $user_identity, $user_level ?>			
												<?php if ( $user_ID ) : 
												if(get_field('download_button_free'))
												{
													echo '<div class="buy-button"><a href="' . do_shortcode(get_field('link_for_download')) . '">' . get_field('download_button_free') . '</a></div>';
												}	?>				
												<?php else : ?>
												
												<?php if(!is_user_logged_in()) { 
													if(get_field('download_button_free'))
													{
														echo '<div class="buy-button"><a href="/wp-login.php?action=register" class="simplemodal-register">' . get_field('download_button_free') . '</a></div>';
													}
												} ?>
												
												<?php endif;
												}?>
															
							</div>
							
							<div class="fix"></div>
							
							<!-- Start Code -->
							<ul>
							<?php
									$postIDs = array();
									$pageChildren = get_pages(
									array (
									'child_of' => $post->post_parent,
									'parent' => $post->post_parent,
									'exclude' => $post->ID
									)
								);
			
								if ( $pageChildren ) {
								  foreach ( $pageChildren as $pageChild ) {
									$postIDs[] = $pageChild->ID;
								  }
								  $args = array(
									'post_type' => 'page',
									'post__in' => $postIDs,
									'orderby'  => 'post_date',
									'order' => 'DESC',
									'posts_per_page' => '3',						
									);
								  query_posts($args);
							
							 if ( have_posts() ) :?>
							 <?php if ( qtrans_getLanguage() == 'en' ) { ?>
								<h2 style="border:0px; margin-top:30px;">Related Items</h2>
								<?php } else { ?>
								<h2 style="border:0px; margin-top:30px;">Похожие товары</h2>
							<?php } ?>
							<?php while ( have_posts() ) : the_post();?>
									<li class="shop-content">
										<div class="parent-tag">
												<?php if ( get_post_meta($post->post_parent, 'icon_parent_tag', true) ) : ?>
													<?php $image_icon = wp_get_attachment_image_src (get_post_meta($post->post_parent, 'icon_parent_tag', true)); ?>
													<img src="<?php echo $image_icon[0]; ?>" />
												<?php endif; ?>
												<?php echo get_the_title($post->post_parent); ?> 
												<?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?>										
										</div>
										<div class="shop-thumb">
											<div class="bottom">
													<?php if ( qtrans_getLanguage() == 'en' ) {
												
												global $user_ID, $user_identity, $user_level ?>			
												<?php if ( $user_ID ) : 
												if(get_field('download_button_free_eng'))
												{
													echo '<div class="buy-button"><a href="' . do_shortcode(get_field('link_for_download')) . '">' . get_field('download_button_free_eng') . '</a></div>';
												}	?>				
												<?php else : ?>
												
												<?php if(!is_user_logged_in()) { 
													if(get_field('download_button_free_eng'))
													{
														echo '<div class="buy-button"><a href="/wp-login.php?action=register" class="simplemodal-register">' . get_field('download_button_free_eng') . '</a></div>';
													}
												} ?>
												
												<?php endif;
												
												} else {
													global $user_ID, $user_identity, $user_level ?>			
												<?php if ( $user_ID ) : 
												if(get_field('download_button_free'))
												{
													echo '<div class="buy-button"><a href="' . do_shortcode(get_field('link_for_download')) . '">' . get_field('download_button_free') . '</a></div>';
												}	?>				
												<?php else : ?>
												
												<?php if(!is_user_logged_in()) { 
													if(get_field('download_button_free'))
													{
														echo '<div class="buy-button"><a href="/wp-login.php?action=register" class="simplemodal-register">' . get_field('download_button_free') . '</a></div>';
													}
												} ?>
												
												<?php endif;
												}?>
											</div>
											<div class="top"><?php the_post_thumbnail('thumbnail'); ?></div>
										</div>
										<a href="<?php the_permalink(); ?>">
											<h3><?php the_title(); ?></h3>
										</a>
										<div class="fix"></div>
	
										<?php if ( qtrans_getLanguage() == 'en' ) {
								
										if(get_field('download_number'))
										{
											echo '<div class="number-downloads-items"><span class="number-downloads">' . do_shortcode(get_field('download_number')) . '</span></div>';
										}
										} else {
											if(get_field('download_number'))
											{
												echo '<div class="number-downloads-items"><span class="number-downloads">' . do_shortcode(get_field('download_number')) . '</span></div>';
											}
										}?>
									</li>					  
							  <?php endwhile;?>
							
							<?php
							else :
								echo wpautop( '' );
							endif;
							?>									
							<div class="fix"></div>						
							<?php }?>
							<?php wp_reset_query(); ?>
							</ul>
						<!-- End Code -->
					<div class="blog-comment-tabber"><?php comments_template( '', true ); ?></div>
					<div class="fix"></div>
				<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>